#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB *no){
  if (!no) return BLACK;
  return (no->cor == RED);
}

ArvoreRB* criarNo(int valor) {
  ArvoreRB* novoNo = malloc(sizeof(ArvoreRB));
  if (novoNo) {
    novoNo->info = valor;
    novoNo->cor = RED;  // Novos nós são inicialmente vermelhos.
    novoNo->esq = NULL;
    novoNo->dir = NULL;
  }
  return novoNo;
}

ArvoreRB* rotacaoEsquerda(ArvoreRB* no) {
  ArvoreRB* temp = no->dir;
  no->dir = temp->esq;
  temp->esq = no;
  temp->cor = no->cor;
  no->cor = RED;
  return temp;
}

ArvoreRB* rotacaoDireita(ArvoreRB* no) {
  ArvoreRB* temp = no->esq;
  no->esq = temp->dir;
  temp->dir = no;
  temp->cor = no->cor;
  no->cor = RED;
  return temp;
}

void trocarCores(ArvoreRB* no) {
  no->cor = RED;
  no->esq->cor = BLACK;
  no->dir->cor = BLACK;
}

ArvoreRB* inserir(ArvoreRB* raiz, int valor) {
  if (raiz == NULL) {
    return criarNo(valor);
  }

  if (valor < raiz->info) {
    raiz->esq = inserir(raiz->esq, valor);
  } else if (valor > raiz->info) {
    raiz->dir = inserir(raiz->dir, valor);
  }

  // Verifica os casos de inserção em um nó-2 folha e nó-3 folha.
  if (eh_no_vermelho(raiz->dir) && !eh_no_vermelho(raiz->esq)) {
    raiz = rotacaoEsquerda(raiz);
  }
  if (eh_no_vermelho(raiz->esq) && eh_no_vermelho(raiz->esq->esq)) {
    raiz = rotacaoDireita(raiz);
  }
  if (eh_no_vermelho(raiz->esq) && eh_no_vermelho(raiz->dir)) {
    trocarCores(raiz);
  }

  return raiz;
}

int buscar(ArvoreRB* raiz, int valor) {
  if (raiz == NULL) {
    return 0;
  } else if (valor < raiz->info) {
    return buscar(raiz->esq, valor);
  } else if (valor > raiz->info) {
    return buscar(raiz->dir, valor);
  } else {
    return 1;
  }
}

void in_order(ArvoreRB *raiz) {
  if (raiz) {
    in_order(raiz->esq);
    printf("%d ", raiz->info);
    in_order(raiz->dir);
  }
}

int main() {
  ArvoreRB *raiz = NULL;
  
  raiz = inserir(raiz, 30);
  raiz = inserir(raiz, 10);
  raiz = inserir(raiz, 20);
  
  in_order(raiz);
  
  return 0;
}
