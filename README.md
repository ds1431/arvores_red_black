# Enunciado

## Árvores Red-Black

### Aula de 03/10/2023
Implemente as operações de inserção e remoção de nós da Árvore Red-Black apresentada na video-aula.
Lembre-se que essas operações devem manter as propriedades da Árvorve Red-Black (balanceamento perfeito de arestas pretas, arestas vermelhas descendentes à esquerda e não possuir duas arestas vermelhas em sequencia).
